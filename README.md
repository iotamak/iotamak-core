# **IoTAMAK : a framework for distributed MAS**

**Name :** Sebastien GOYON  
**Group :** M1 CSA 2021-2022, UT3 Paul Sabatier

**Internship tutor :** Guilhem MARCILLAUD, IRIT SMAC team.

- [**IoTAMAK : a framework for distributed MAS**](#iotamak--a-framework-for-distributed-mas)
  - [**Introduction**](#introduction)
    - [**Definition**](#definition)
    - [**Context**](#context)
    - [**Problem and objective**](#problem-and-objective)
    - [**Solution**](#solution)
    - [**Project management**](#project-management)
  - [**Development**](#development)
    - [**IoTAMAK core**](#iotamak-core)
      - [**Code structure** :](#code-structure-)
      - [**MAS behavious**](#mas-behavious)
      - [**Version**](#version)
    - [**IoTAMAK UI**](#iotamak-ui)
  - [**Network**](#network)
    - [**Raspberry network**](#raspberry-network)
    - [**Server network**](#server-network)
  - [**Bibliography**](#bibliography)


## **Introduction**

### **Definition**

MAS (multi-agent system):
> A multi-agent system (MAS or "self-organized system") is a computerized system composed of multiple interacting intelligent agents.
> Multi-agent systems can solve problems that are difficult or impossible for an individual agent or a monolithic system to solve.
> Intelligence may include methodic, functional, procedural approaches, algorithmic search or reinforcement learning.


### **Context**

The SMAC team in Irit developed a tool called Amak that help scientist to develop MAS system in Java.
The wished to extend the tool in multiple programing language, 
last year I participated in the conception and development of Py-AMAK (Python) during my internship,
an extension of AMAK (Java). Other version of AMAK exist in C# and C++.

This year the goal of the internship is to develop IoTAMAK, that will extend the application of Py-AMAK to use network and multiple device. 

### **Problem and objective**

So far all Amak system can only be used to simulate MAS system, but they canno't be use with an agent that would exist in our world.
IoTAMAK goal to connect any devices to the system as agent(s) and use the network to communicate. 

Notable difference between IoTAMAK and Py-AMAK on the interface :  
 * The structure need to be done in a more pythonic way (no getter/setter, no public/private attribute)
 * The UI need to be a web app powered by a server

> The goal of this project is to produce a wab-app to supervise remotely experiments distributed on multiple device for researcher in MAS of the IRIT. The application need to give in real time information to the user of the state of each devise / agent (activity, result..). The general architecture  of the system is in 3 part :  
>  1. Some devises that can work (Raspberry pi 3b)
>  2. A control device collecting information (server)
>  3. An application that can show the server information according to the user need.  
> 
> This project will be focused on the development of a system that can distribute a SMA define by AMAK on multiple raspberry PI. Once distributed, the server can start and stop the experiment. The 2nd goal of this project is to build a network architecure between those raspberry so each Agent can communicate between them and with the manager.

Constraint : 
 * Communication between agent : MQTT 
 * Language : Python
 * Follow the MVC patern to separate the UI with the core system, this mean that the core could work on his own, and a UI is not require. 

### **Solution**

The project will be split into 2 parts :
 * IoTAMAK-core : a python module (that can be easily installed with pip) that provide all the basic method required to build a MAS experiment
 * IoTAMAK UI : a django based server that can interact with any experiment developed with IoTAMAK-core.

### **Project management** 

Tool : 
 * Gitlab
 * Trello
 * Discord

## **Development**

### **IoTAMAK core**

The classes that the developer will interact with (Agent / Amas / Environment) need to be very similar to the Py-AMAK one, this is why the class looks very similar. 

#### **Code structure** :

In AMAK a SMA is composed of 4 main structure : 
 * Scheduler : it's a role is to make sure that everything is working in the right order, it's also play a huge role with the interface between the UI and the experiment, being able to pause it, or closing everyting.
 * Agent : an agent can have various comportment that need to be define
 * Amas : it's a superstructure, above the agents that provide convenient way to add, remove or make communication between agents
 * Environment : a place where the agent live.

```mermaid
classDiagram

    class Amas{
        int next_id
        List~Cmd~ agent_cmd
        List~Dict~ agents_metrics
        on_initialization()
        on_initial_agents_creation()
        add_agent()
        push_agent()
        agent_log()
        agent_metric()
        on_cycle_begin()
        on_cycle_end()
        run()
    }

    class Agent{
        List~Dict~ neighbors
        List~Dict~ next_neighbors
        on_initialization()
        on_cycle_begin()
        on_cycle_end()
        add_neighbor()
        log(message)
        publish()
        on_perceive()
        on_decide()
        on_act()
        send_metric() Dict
        run()
    }

    class Env{
        on_initialization()
        on_cycle_begin()
        on_cycle_end()
        run()
    }

    class Scheduler

    class MqttClient{
        ~Paho mqtt client~ client
        subscribe(topic, fun)
        publish(topic, message)
    }

    class Schedulable{
        int exit_bool
        int nbr_cycle
        int wait_delay
        int wake_up_token
        wake_up()
        wait()
        exit_procedure()
    }

    class SSHClient


    MqttClient <|-- Schedulable
    Schedulable <|-- Scheduler
    Schedulable <|-- Agent
    Schedulable <|-- Env
    Schedulable <|-- Amas
    SSHClient <|-- Amas
```

#### **MAS behavious**

```mermaid
graph TD
    
    subgraph first_part:
      amas_metrics(Amas : publish all stored metrics to database)
      amas_begin(Amas : on_cycle_begin)
      amas_not(Amas : notify scheduler)
      env_begin(Env : on_cycle_begin)
      env_not(Env : notify scheduler)

      amas_metrics --> amas_begin
      amas_begin --> amas_not
      env_begin --> env_not
    end
    
    subgraph main_part:
      agent1_begin(Agent 1 : on_cycle_begin)
      agent1_perceive(Agent 1 : on_perceive)
      agent1_decide(Agent 1 : on_decide)
      agent1_act(Agent 1 : on_act)
      agent1_finish(Agent 1 : on_cycle_end)
      agent1_metric(Agent 1 : send metrics)
      agent1_sche(Agent 1 : notify scheduler)

      agent1_begin --> agent1_perceive
      agent1_perceive --> agent1_decide
      agent1_decide --> agent1_act
      agent1_act --> agent1_finish
      agent1_finish --> agent1_metric
      agent1_metric --> agent1_sche

      agent_mid(...)
      
      agentn_begin(Agent n : on_cycle_begin)
      agentn_perceive(Agent n : on_perceive)
      agentn_decide(Agent n : on_decide)
      agentn_act(Agent n : on_act)
      agentn_finish(Agent n : on_cycle_end)
      agentn_metric(Agent n : send metrics)
      agentn_sche(Agent n : notify scheduler)

      agentn_begin --> agentn_perceive
      agentn_perceive --> agentn_decide
      agentn_decide --> agentn_act
      agentn_act --> agentn_finish
      agentn_finish --> agentn_metric
      agentn_metric --> agentn_sche

    end
    
    subgraph last_part:
      amas_finish(Amas : on_cycle_end)
      amas_noti(Amas : notify scheduler)
      env_finish(Env : on_cycle_end)
      env_noti(Env : notify scheduler)

      amas_finish --> amas_noti
      env_finish --> env_noti
    end

    scheduler_not_first(Scheduler : Notify Amas/env)
    scheduler_wait_first(Scheduler : Wait Amas/env)
    scheduler_not_main(Scheduler : Notify all Agents)
    scheduler_wait_main(Scheduler : Wait all agents)
    scheduler_not_last(Scheduler : Notify Amas/env)
    scheduler_wait_last(Scheduler : Wait Amas/env)


    scheduler_not_first --> amas_metrics
    scheduler_not_first --> env_begin
    amas_not --> scheduler_wait_first
    env_not --> scheduler_wait_first

    scheduler_wait_first --> scheduler_not_main

    scheduler_not_main --> agent1_begin
    scheduler_not_main --> agent_mid
    scheduler_not_main --> agentn_begin
    agent1_sche --> scheduler_wait_main
    agent_mid --> scheduler_wait_main
    agentn_sche --> scheduler_wait_main

    scheduler_wait_main --> scheduler_not_last
    
    scheduler_not_last --> amas_finish
    scheduler_not_last --> env_finish
    env_noti --> scheduler_wait_last
    amas_noti --> scheduler_wait_last
```

#### **Version**

**0.0.1 :**
 * Basic implementation of the system

**0.0.2 :**

Clean code : 
 * Provide way better interface for the developper

Amas : 
 * Optimize metric publish to the database

SSH Client : 
 * will try to connect once again if it have failed

**0.0.3 :**  
Amas :
 * Add : method agent_neighbour that publish the metric of agent2 to agent 1

Schedulable & Scheduler :
 * Greatly improve the wait model, now using threading semaphore

**0.0.4**  
Feature :
 * Amas, Env and Agent now take only 1 argument to simplify arg managment for the user. (Not compatible with 0.0.3)
 * It's now possible to seed the experiment.
 * Scheduler will print exec time

Requirements.txt / Setup.py :
 * add requirement version

Known bug :
 * Scheduler : auto mode pause seem to lock the scheduler in a state where it's not possible to interact anymore with it.

### **IoTAMAK UI**

The Web application will be powered by a django server.


## **Network**

In order for the system to work a network architecture need to be conceptualize. 

The communication between agents need to be MQTT.  

There are 3 actore in this system : a server, some user and somre device(raspberry) that execute agents. 

```mermaid

graph LR

raspberry_1
raspberry_n
client((User))

git[(Gitlab : iotamak-core)]

subgraph server
  django
  broker
end

broker-- MQTT 1883---raspberry_1
django-- SSH/SFTP: 22---raspberry_1
django-- ping---raspberry_1
broker-- MQTT 1883---raspberry_n
django-- SSH/SFTP: 22---raspberry_n
django-- ping---raspberry_n

django-- Git : pull --- git
raspberry_n-- Git : pull ---git
raspberry_1-- Git : pull ---git

client --> django
```

### **Raspberry network**
From the point of view of a raspberry the network look like this : 
```mermaid
graph LR

subgraph raspberry
  direction TB
  os(OS)
  client_1
  client_2
  client_n

  os-- nohup/kill---client_1
  os-- nohup/kill---client_2
  os-- nohup/kill---client_n
end

server((Server))

git((Gitlab : iotamak-core))

server-- MQTT : 1883---client_1
server-- MQTT : 1883---client_2
server-- MQTT : 1883---client_n
server-- ping--- os
server-- SSH/SFTP: 22--- os
git-- Git : pull--- os
```

We can see that beside the agent communicate not only with MQTT to the server but also with ssh and sftp in order to share the experiment and control the experiment in case of abnormal beharvior(kill agents for example, or see the error stack).

### **Server network**

```mermaid
graph LR

subgraph Server
  direction TB
  
  subgraph DjangoApp
    ihm[Mqtt : IHM]
    django[Django server]
    
  end

  subgraph Experiment
    amas[Amas]
    env[Environment]
    scheduler[Scheduler]
  end

  cache((Cache server : Redis))
  broker((Broker : Mosquitto))
  database[(Database : PostgreSQL)]

  django-- Port : 6379 ---cache

  django-- popen / kill ---amas
  django-- popen / kill ---env
  django-- popen / kill ---scheduler
  django-- Port : 5432 ---database

  amas-- MQTT : 1883---broker
  env-- MQTT : 1883---broker
  scheduler-- MQTT : 1883---broker
  ihm-- MQTT : 1883---broker


end

raspberry
git((Gitlab : iotamak-core))
User 

django-- SSH/SFTP: 22 / ping ---raspberry
django-- Git : pull ---git
amas-- SSH: 22 ---raspberry
broker-- MQTT : 1883---raspberry
User --> django
```

The server is composed of multiple componant : 
 * A MQTT broker (Mosquitto is used here) : to hold the communication between the agent, amas, scheduler, and env.
 * A Django application composed of the main application and a MQTT client to ineteract with the experiment
 * An experiment, composed of a Scheduler, an environment and an amas. 
 * A database : since the base Django database (Sqlite3) didn't meet the requirement for the system and external database is require, PoqstgreSQL.
 * A cache server Redis : in order to have real time graph and canvas, a cahe server was needed. 

## **Bibliography**

 1. Smac : https://www.irit.fr/en/departement/dep-interaction-collective-intelligence/smac-team/
 2. Irit : https://www.irit.fr/en/home/  
 3. MAS definition : https://en.wikipedia.org/wiki/Multi-agent_system
