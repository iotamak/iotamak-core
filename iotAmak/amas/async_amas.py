import pathlib
import sys
import json
from ast import literal_eval
from time import sleep
from typing import List


sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.amas.base_amas import BaseAmas
from iotAmak.base.async_controlable import AsyncControlable
from iotAmak.ssh_module.remote_client import RemoteClient
from iotAmak.ssh_module.ssh_client import Cmd


class AsyncAmas(AsyncControlable, BaseAmas):
    """
    Amas class
    """

    def __init__(self, arguments: str) -> None:

        arguments = json.loads(arguments)

        broker_ip: str = arguments.get("broker_ip")
        clients: str = arguments.get("clients")
        seed: int = int(arguments.get("seed"))
        broker_username: str = str(arguments.get("broker_username"))
        broker_password: str = str(arguments.get("broker_password"))
        iot_path: str = str(arguments.get("iot_path"))
        wait_delay: float = float(arguments.get("wait_delay"))
        experiment_folder: str = str(arguments.get("experiment_folder"))

        true_client = [RemoteClient(i.get("hostname"), i.get("user"), i.get("password")) for i in literal_eval(clients)]

        AsyncControlable.__init__(
            self,
            broker_ip,
            "Amas",
            broker_username,
            broker_password,
            wait_delay
        )
        BaseAmas.__init__(self,
                          broker_ip,
                          broker_username,
                          broker_password,
                          seed,
                          iot_path,
                          true_client,
                          experiment_folder)

        self.push_agent()

    def on_metric(self) -> None:
        """
        This method will be executed everytime a metric is send
        """
        pass

    def add_agent(
            self,
            client_ip: str = None,
            agent_name: str = "agent.py",
            args: List = None
    ) -> None:
        """
        Function that need to be called to create a new agent
        :param experience_name: name of the experience folder
        :param client_ip: if the agent should be created in a specific device, you can specify an ip address,
        otherwise the Amas will try to share the work between the devices
        :param agent_name: if using multiple kind of agent, you can specify the relative path in the experiment
        directory to the agent file to use
        :param args: if any argument is needed to initiate the new agent
        :return: None
        """
        if args is None:
            args = []

        arg_dict = {
            "broker_ip": str(self.broker_ip),
            "seed": self.seed,
            "identifier": self.next_id,
            "broker_username": self.broker_username,
            "broker_password": self.broker_password,
            "wait_delay": self.wait_delay
        }

        command = "nohup python "
        command += "\'" + self.iot_path + self.experiment_folder + "/" + agent_name + "\' \'"
        command += json.dumps(arg_dict) + "\' "
        for arg in args:
            command += str(arg) + " "
        command += "&"

        if client_ip is None:
            # find the most suitable pi
            i_min = 0
            for elem in range(len(self.clients)):
                if len(self.agents_cmd[i_min]) > len(self.agents_cmd[elem]):
                    i_min = elem
            self.agents_cmd[i_min].append(Cmd(command))
        else:
            have_found = False
            for i_client in range(len(self.clients)):
                if self.clients[i_client].hostname == client_ip:
                    self.agents_cmd[i_client].append(Cmd(command))
                    have_found = True
                    break
            if not have_found:
                self.agents_cmd[0].append(Cmd(command))

        self.subscribe("agent/" + str(self.next_id) + "/metric", self.agent_metric)

        self.client.publish("amas/agent/new", self.next_id)
        self.next_id += 1

    def run(self) -> None:
        """
        Main method of the client
        """

        while not self.exit_bool:
            # wait to be unpause
            self.wait()

            # check the need to exit
            if self.exit_bool:
                return

            self.publish("amas/graph", str(self.to_graph()))
            self.publish("amas/all_metric", str(self.agents_metric))
            self.behaviour()

            sleep(self.wait_delay)
            self.nbr_cycle += 1