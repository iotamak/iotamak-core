import json
import random
import sys
import pathlib
from ast import literal_eval
from typing import List, Dict

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.ssh_module.ssh_client import SSHClient, Cmd
from iotAmak.ssh_module.remote_client import RemoteClient


class BaseAmas(SSHClient):

    def __init__(
            self,
            broker_ip: str,
            broker_username: str,
            broker_password: str,
            seed: int,
            iot_path: str,
            clients: List[RemoteClient],
            experiment_folder: str
    ):
        self.broker_ip: str = broker_ip
        self.broker_username: str = broker_username
        self.broker_password: str = broker_password

        self.experiment_folder = experiment_folder

        self.next_id: int = 0

        self.seed: int = seed
        random.seed(self.seed)

        self.agents_cmd: List[List[Cmd]] = [[] for _ in range(len(clients))]

        SSHClient.__init__(self, clients, iot_path)

        self.on_initialization()
        self.on_initial_agents_creation()
        self.agents_metric: List[Dict] = [{} for _ in range(self.next_id)]
        self.agent_info: List[Dict] = [{} for _ in range(self.next_id)]

    def on_initialization(self) -> None:
        """
        This method will be executed at the end of __init__()
        """
        pass

    def on_initial_agents_creation(self) -> None:
        """
        Convenient method to initially create the agents, is called at the end of initialization
        """
        pass

    def add_agent(
            self,
            client_ip: str = None,
            agent_name: str = "agent.py",
            args: List = None
    ) -> None:
        """
        Function that need to be called to create a new agent
        :param experience_name: name of the experience folder
        :param client_ip: if the agent should be created in a specific device, you can specify an ip address,
        otherwise the Amas will try to share the work between the devices
        :param agent_name: if using multiple kind of agent, you can specify the relative path in the experiment
        directory to the agent file to use
        :param args: if any argument is needed to initiate the new agent
        :return: None
        """
        if args is None:
            args = []

        arg_dict = {
            "broker_ip": str(self.broker_ip),
            "seed": self.seed,
            "identifier": self.next_id,
            "broker_username": self.broker_username,
            "broker_password": self.broker_password
        }

        command = "nohup python "
        command += "\'" + self.iot_path + self.experiment_folder + "/" + agent_name + "\' \'"
        command += json.dumps(arg_dict) + "\' "
        for arg in args:
            command += str(arg) + " "
        command += "&"

        if client_ip is None:
            # find the most suitable pi
            i_min = 0
            for elem in range(len(self.clients)):
                if len(self.agents_cmd[i_min]) > len(self.agents_cmd[elem]):
                    i_min = elem
            self.agents_cmd[i_min].append(Cmd(command))
            self.client.publish("amas/agent/new", str({"id": self.next_id, "ip": i_min}))
        else:
            have_found = False
            for i_client in range(len(self.clients)):
                if self.clients[i_client].hostname == client_ip:
                    self.agents_cmd[i_client].append(Cmd(command))
                    self.client.publish("amas/agent/new", str({"id": self.next_id, "ip": i_client}))
                    have_found = True
                    break
            if not have_found:
                self.agents_cmd[0].append(Cmd(command))
                self.client.publish("amas/agent/new", str({"id": self.next_id, "ip": 0}))

        self.subscribe("agent/" + str(self.next_id) + "/metric", self.agent_metric)

        self.next_id += 1

    def push_agent(self) -> None:
        """
        Method used to start new agent trough ssh
        """
        for i_client in range(len(self.clients)):
            print(" Ip : ", self.clients[i_client].hostname)
            self.run_cmd(i_client, self.agents_cmd[i_client])
        print("Amas, push agent done")

    def agent_neighbour(self, id_agent1: int, id_agent2: int) -> None:
        """
        share the metric of agent 2 to agent 1
        """
        self.publish("amas/agent/" + str(id_agent1) + "/neighbor", str(self.agents_metric[id_agent2]))

    def agent_metric(self, client, userdata, message) -> None:
        """
        Called when the amas receive new metrics from any agent
        """
        # print("Received message ", literal_eval(message.payload.decode("utf-8")), " on topic '" + message.topic)
        result = literal_eval(message.payload.decode("utf-8"))
        agent_id = result.get("id")
        self.agents_metric[agent_id] = result.get("metric")
        self.agent_info[agent_id] = result.get("amas")

    def to_graph(self) -> dict:
        pass
