import json
import sys
import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))

from iotAmak.ssh_module.remote_client import RemoteClient


def read_ssh(path):
    with open(path, 'r', encoding='utf-8') as f:
        ssh_dict = json.load(f)

    res = []
    for client in ssh_dict.get("clients_ssh"):
        res.append(RemoteClient(
            hostname=client.get("hostname"),
            user=client.get("user"),
            password=client.get("password")
        ))
    return res


def read_broker(path):
    with open(path, 'r', encoding='utf-8') as f:
        dict = json.load(f)

    return dict.get("broker")
