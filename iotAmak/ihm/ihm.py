from os import path
from subprocess import Popen
from time import sleep

import platform
import subprocess
import sys
import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parent))

from iotAmak.ihm.confi_reader import read_ssh, read_broker
from iotAmak.base.mqtt_client import MqttClient
from iotAmak.ssh_module.ssh_client import SSHClient, Cmd


class Ihm(MqttClient, SSHClient):

    def __init__(self, config_path, experiment_name, version="0.0.1"):
        self.version = version
        self.experiment_name = experiment_name
        self.broker_ip = read_broker(config_path)
        MqttClient.__init__(self, self.broker_ip, "Ihm")
        SSHClient.__init__(self, read_ssh(config_path))

        self.experiment_loaded = False
        self.ping_is_true = False

    def loading(self):
        print("[LOADING]")
        print("Check experiment:")
        print(" | -> agent.py : ", path.exists("../agent/agent.py"))
        print(" | -> amas.py : ", path.exists("../amas/amas.py"))
        print(" | -> env.py : ", path.exists("./env.py"))
        if path.exists("../agent/agent.py") and path.exists("../amas/amas.py") and path.exists("./env.py"):
            self.experiment_loaded = True
            print("Experiment loaded")
        else:
            print("[WARNING] experiment isn't conform with the expected format, starting won't be possible")

    def ping(self):
        print("[PING]")
        print("Checking connection to clients in config.json")
        res = True
        for client in self.clients:
            param = '-n' if platform.system().lower() == 'windows' else '-c'
            command = ['ping', param, '1', client.hostname]

            result = subprocess.call(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0
            res = res and result

            print("Hostname :", client.hostname, " Responded : ", result)
        if res:
            print("Connection established")
            self.ping_is_true = True
        else:
            print("[WARNING] Some client are unavailable, update the config file")

    def agent(self):
        print("[AGENT]")
        print("Checking if an experiment is already running")

        commands = [
            Cmd(
                cmd="ps -ef | grep 'python '"
            )]

        for i_client in range(len(self.clients)):
            print("Hostname :", self.clients[i_client].hostname, " User :", self.clients[i_client].user)
            self.run_cmd(i_client, commands)

        print(
            "If any agent is currently running, you should" +
            "kill them before starting the application with the command 'kill'"
        )

    def run(self):

        exit_bool = False

        self.loading()
        self.ping()

        while not exit_bool:

            print("\n")
            cmd = input(">")

            # Envoie un signal a tout le monde pour exit
            if cmd.lower() == "exit":
                self.client.publish("ihm/exit")
                exit_bool = True

            # cherche tout les potentiel agents sur les raspberry et les tuent
            if cmd.lower() == "kill":

                commands = [
                    Cmd(
                        cmd="ps -ef | grep 'python '",
                        prefix="[BEFORE]"
                    ),
                    Cmd(
                        cmd="for pid in $(ps -ef | grep 'python ' | awk '{print $2}'); do kill $pid; done",
                        do_print=False
                    ),
                    Cmd(
                        cmd="ps -ef | grep 'python '",
                        prefix="[AFTER]"
                    )
                ]

                for i_client in range(len(self.clients)):
                    print("Hostname :", self.clients[i_client].hostname, " User :", self.clients[i_client].user)
                    self.run_cmd(i_client, commands)

            # automatise la mise a jour de l'exerience sur les raspberry
            if cmd.lower() == "update":
                commands = [
                    Cmd(
                        cmd="cd Desktop/mqtt_goyon/iotamak-core"
                    ),
                    Cmd(
                        cmd="git pull"
                    ),
                    Cmd(
                        cmd="git checkout main"
                    ),
                    Cmd(
                        cmd="python3 -m pip install --force-reinstall dist/iotAmak-"+self.version+"-py3-none-any.whl"
                    ),
                    Cmd(
                        cmd="cd ../../../"
                    ),
                    Cmd(
                        cmd="rm -r Desktop/mqtt_goyon/example/" + self.experiment_name
                    )

                ]
                for i_client in range(len(self.clients)):
                    print("Hostname :", self.clients[i_client].hostname, " User :", self.clients[i_client].user)
                    self.run_cmd(i_client, commands)

                self.update(self.experiment_name, str(pathlib.Path().resolve()))

            if cmd.lower() == "agent":
                self.agent()

            # Crée les processus amas/env/scheduler de l'experience chargé
            if cmd.lower() == "start":

                if self.experiment_loaded and self.ping_is_true:

                    # start subprocess scheduler
                    p1 = Popen([sys.executable, './scheduler.py', self.broker_ip])
                    sleep(1)
                    # start subprocess amas
                    send_client = [c.to_send() for c in self.clients]
                    p2 = Popen([sys.executable, './amas.py', self.broker_ip, str(send_client)])
                    # start subprocess env
                    p3 = Popen([sys.executable, './env.py', self.broker_ip])

            if cmd.lower() == "pause":
                self.client.publish("ihm/pause")

            if cmd.lower() == "unpause":
                self.client.publish("ihm/unpause")

            if cmd.lower() in ["s", "step"]:
                self.client.publish("ihm/step")

        self.client.publish("ihm/step")
        sleep(2)
