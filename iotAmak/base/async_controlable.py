"""
MQTT client class file
"""
import sys
import pathlib
from threading import Semaphore
from time import sleep

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.base.mqtt_client import MqttClient


class AsyncControlable(MqttClient):
    """
    Base class to any instance that need to interact with the broker
    """

    def __init__(
            self,
            broker_ip: str,
            client_id: str,
            broker_username: str,
            broker_password: str,
            wait_delay: float
    ):
        MqttClient.__init__(self, broker_ip, client_id, broker_username, broker_password)

        # exit
        self.exit_bool: bool = False
        self.subscribe("ihm/exit", self.exit_procedure)

        # pause
        self.paused: bool = True
        self.pause_semaphore = Semaphore(0)
        self.subscribe("ihm/pause", self.pause)
        self.subscribe("ihm/unpause", self.unpause)

        # time to wait
        self.wait_delay: float = wait_delay

        self.nbr_cycle: int = 0

    def exit_procedure(self, client, userdata, message) -> None:
        """
        Called by the Ihm to exit as soon as possible
        """
        self.exit_bool = True

    def pause(self, client, userdata, message) -> None:
        """
        Function called when the IHM pause the scheduler
        """
        self.paused = True

    def unpause(self, client, userdata, message) -> None:
        """
        Function called when the IHM unpause the scheduler
        """
        self.paused = False
        self.pause_semaphore.release()

    def wait(self) -> None:
        """
        If the element is paused, wait until unpause was received
        """
        if not self.paused:
            return
        self.pause_semaphore.acquire()

    def behaviour(self) -> None:
        """
        method to override
        """
        return

    def run(self) -> None:
        """
        Main method of the client
        """

        while not self.exit_bool:
            # wait to be unpause
            self.wait()

            # check the need to exit
            if self.exit_bool:
                return

            self.behaviour()

            sleep(self.wait_delay)
            self.nbr_cycle += 1
