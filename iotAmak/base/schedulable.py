"""
Tool class that implement basic interaction that help to finish processes
"""

import sys
import pathlib
import threading

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))

from iotAmak.base.mqtt_client import MqttClient


class Schedulable(MqttClient):
    """
    Base class for Agent/Amas/Env/scheduler
    """

    def __init__(self, broker_ip: str, client_id: str, broker_username: str, broker_password: str):
        MqttClient.__init__(self, broker_ip, client_id, broker_username, broker_password)

        self.exit_bool: bool = False
        self.subscribe("ihm/exit", self.exit_procedure)

        self.nbr_cycle: int = 0

        self.semaphore = threading.Semaphore(0)

    def wake_up(self, client, userdata, message) -> None:
        """
        Called by the scheduler to wake up the schedulable
        """
        self.semaphore.release()

    def wait(self) -> None:
        """
        Basic wait method
        """
        self.semaphore.acquire()

    def exit_procedure(self, client, userdata, message) -> None:
        """
        Called by the Ihm to exit as soon as possible
        """
        self.exit_bool = True
