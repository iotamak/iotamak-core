"""
Environment class
"""
import json
import random
import sys
import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parent))
from iotAmak.env.base_env import BaseEnv
from iotAmak.base.schedulable import Schedulable


class Environment(Schedulable, BaseEnv):
    """
    Environment class
    """

    def __init__(self, arguments: str) -> None:

        arguments = json.loads(arguments)

        broker_ip: str = arguments.get("broker_ip")
        seed: int = int(arguments.get("seed"))
        broker_username: str = str(arguments.get("broker_username"))
        broker_password: str = str(arguments.get("broker_password"))

        Schedulable.__init__(self, broker_ip, "Env", broker_username, broker_password)
        self.subscribe("scheduler/schedulable/wakeup", self.wake_up)
        BaseEnv.__init__(self, seed)
        self.client.publish("env/action_done", "")

    def on_cycle_begin(self) -> None:
        """
        This method will be executed at the start of each cycle
        """
        pass

    def on_cycle_end(self) -> None:
        """
        This method will be executed at the end of each cycle
        """
        pass

    def run(self) -> None:
        """
        Main function of the env
        """
        while not self.exit_bool:
            self.wait()
            if self.exit_bool:
                return

            self.on_cycle_begin()
            self.client.publish("env/action_done", "")

            # agent cycle

            self.wait()
            self.on_cycle_end()
            self.client.publish("env/action_done", "")

            self.nbr_cycle += 1
