"""
Environment class
"""
import json
import sys
import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.env.base_env import BaseEnv
from iotAmak.base.async_controlable import AsyncControlable


class AsyncEnvironment(BaseEnv, AsyncControlable):
    """
    Environment class
    """

    def __init__(self, arguments: str) -> None:

        arguments = json.loads(arguments)

        broker_ip: str = arguments.get("broker_ip")
        seed: int = int(arguments.get("seed"))
        broker_username: str = str(arguments.get("broker_username"))
        broker_password: str = str(arguments.get("broker_password"))
        wait_delay: float = float(arguments.get("wait_delay"))

        AsyncControlable.__init__(
            self,
            broker_ip,
            "Env",
            broker_username,
            broker_password,
            wait_delay
        )
        BaseEnv.__init__(self, seed)
