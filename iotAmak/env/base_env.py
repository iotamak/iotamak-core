import random


class BaseEnv:
    def __init__(self, seed: int) -> None:
        random.seed(seed + 1)
        self.on_initialization()

    def on_initialization(self) -> None:
        """
        This method will be executed at the end of __init__()
        """
        pass
