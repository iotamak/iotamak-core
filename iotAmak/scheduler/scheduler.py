"""
Scheduler class file
"""
from ast import literal_eval
from threading import Semaphore
from time import sleep, time

import sys
import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.base.schedulable import Schedulable


class Scheduler(Schedulable):
    """
    Scheduler class, it's role is to make sure the amas, the env and all the agents stay in sync
    """

    def __init__(self, broker_ip: str, broker_username: str, broker_password: str) -> None:

        Schedulable.__init__(self, broker_ip, "Scheduler", broker_username, broker_password)
        self.paused = True

        self.nbr_agent: int = 0
        self.subscribe("amas/agent/new", self.update_nbr_agent)
        self.subscribe("amas/action_done", self.update_schedulable)
        self.subscribe("env/action_done", self.update_schedulable)
        self.subscribe("ihm/step", self.step)
        self.subscribe("ihm/pause", self.pause)
        self.subscribe("ihm/unpause", self.unpause)

        self.agent_semaphore = Semaphore(0)
        self.schedulable_semaphore = Semaphore(0)
        self.ihm_semaphore = Semaphore(0)

        print("Init done")

    def pause(self, client, userdata, message) -> None:
        """
        Function called when the IHM pause the scheduler
        """
        self.paused = True

    def unpause(self, client, userdata, message) -> None:
        """
        Function called when the IHM unpause the scheduler
        """
        self.paused = False
        self.ihm_semaphore.release()

    def step(self, client, userdata, message) -> None:
        """
        Function called by the IHM when the scheduler is in Step by Step mode
        """
        self.ihm_semaphore.release()

    def update_schedulable(self, client, userdata, message) -> None:
        """
        Function called whenever the amas/env have finished an action and is waiting
        """
        self.schedulable_semaphore.release()
        # print("__Schedulable is waiting")

    def update_nbr_agent(self, client, userdata, message) -> None:
        """
        Called when a new agent is added to the system
        param message: id of the new agent
        """
        self.nbr_agent += 1
        self.subscribe(
            "agent/" + str(literal_eval(message.payload.decode("utf-8")).get("id")) + "/cycle_done",
            self.agent_done)

        # print("__Update agent : ", self.nbr_agent, str(message.payload.decode("utf-8")))

    def agent_done(self, client, userdata, message) -> None:
        """
        Called whenever an agent have done an action and is waiting
        """
        self.agent_semaphore.release()
        # print("__Agent done")

    def wait_agent(self) -> None:
        """
        Called when the scheduler is waiting for all agent do have finished their action
        """
        for _ in range(self.nbr_agent):
            self.agent_semaphore.acquire()

    def wait_schedulable(self) -> None:
        """
        Called when the scheduler is waiting for both amas and env to have finished their action
        """
        for _ in range(2):
            self.schedulable_semaphore.acquire()

    def wait_ihm(self) -> None:
        """
        Called when the scheduler is waiting for an action of the IHM
        """
        if not self.paused:
            return
        self.ihm_semaphore.acquire()

    def first_part(self) -> None:
        """
        first part of a cycle : Amas/Env on_cycle_begin
        """
        self.client.publish("scheduler/schedulable/wakeup", "")
        # Amas on cycle begin
        # Environment on cycle begin
        self.wait_schedulable()

    def main_part(self) -> None:
        """
        main part of a cycle : Agent cycle
        """
        self.client.publish("scheduler/agent/wakeup", "")
        # Agent doing cycle
        self.wait_agent()

    def last_part(self) -> None:
        """
        last part of a cycle : Amas/Env on_cycle_end
        """
        self.client.publish("scheduler/schedulable/wakeup", "")
        # Amas on cycle end
        # Environment on cycle end
        self.wait_schedulable()

    def run(self) -> None:
        """
        Main function of the scheduler
        """

        # wait that all schedulable have init
        print("Waiting schedulable")
        self.wait_schedulable()
        # Wait that all agent have init
        print("Waiting agents :", self.nbr_agent)
        self.wait_agent()

        while not self.exit_bool:
            print("Cycle : ", self.nbr_cycle)

            self.wait_ihm()
            if self.exit_bool:
                print("Exit")
                break

            print("-First part")
            start_time = time()
            self.first_part()
            print("--- %s seconds ---" % (time() - start_time))
            print("-Main part")
            start_time = time()
            self.main_part()
            print("--- %s seconds ---" % (time() - start_time))
            print("-Last part")
            start_time = time()
            self.last_part()
            print("--- %s seconds ---" % (time() - start_time))

            self.nbr_cycle += 1
            self.publish("scheduler/cycledone", "")

        print("Exit loop")
        self.client.publish("scheduler/schedulable/wakeup", "")
        self.client.publish("scheduler/agent/wakeup", "")
        
        sleep(5)
        print("Done")
