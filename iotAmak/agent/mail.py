from typing import Any


class Mail:
    """
    Mail class used by the communicating agent
    """

    def __init__(self, sender_id: int, date: int, payload: Any) -> None:

        self.sender_id: int = sender_id
        self.date: int = date
        self.payload: Any = payload
