"""
Agent class file
"""
import json
import sys
import pathlib
from typing import Dict

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.base.schedulable import Schedulable
from iotAmak.agent.base_agent import BaseAgent


class Agent(Schedulable, BaseAgent):
    """
    base class for agent
    """

    def __init__(self, arguments: str) -> None:

        arguments: Dict = json.loads(arguments)

        broker_ip: str = arguments.get("broker_ip")
        identifier: int = int(arguments.get("identifier"))
        seed: int = int(arguments.get("seed"))
        broker_username: str = str(arguments.get("broker_username"))
        broker_password: str = str(arguments.get("broker_password"))

        Schedulable.__init__(self, broker_ip, "Agent" + str(identifier), broker_username, broker_password)
        BaseAgent.__init__(self, identifier, seed)

        self.subscribe("scheduler/agent/wakeup", self.wake_up)

        self.on_initialization()

        self.publish("cycle_done", "")

    def publish(self, topic: str, message) -> None:
        """
        publish a message on the topic
        :param topic: str
        :param message: content of the message
        """
        self.client.publish("agent/" + str(self.id) + "/" + topic, message)

    def run(self) -> None:
        """
        Main method of the agent
        """
        while not self.exit_bool:

            self.wait()
            if self.exit_bool:
                return

            self.on_cycle_begin()

            self.on_perceive()

            self.on_decide()

            self.on_act()

            self.on_cycle_end()

            self.publish("metric", str({"id": self.id, "metric": self.send_metric(), "amas": self.amas_data()}))
            self.publish("canvas", str(self.to_canvas()))
            self.publish("cycle_done", "")
            self.nbr_cycle += 1
