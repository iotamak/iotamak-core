import pathlib
import sys

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.agent.agent import Agent
from iotAmak.agent.base_communicating_agent import BaseCommunicatingAgent


class CommunicatingAgent(Agent, BaseCommunicatingAgent):
    """
    Agent class that can communicate
    """

    def __init__(self, arguments: str) -> None:
        Agent.__init__(self, arguments)
        BaseCommunicatingAgent.__init__(self)
