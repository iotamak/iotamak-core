"""
AsyncAgent class file
"""
import pathlib
import sys
import json
from typing import Dict

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.base.async_controlable import AsyncControlable
from iotAmak.agent.base_agent import BaseAgent


class AsyncAgent(AsyncControlable, BaseAgent):
    """
    Async class for agent
    """

    def __init__(self, arguments: str) -> None:

        arguments: Dict = json.loads(arguments)

        broker_ip: str = arguments.get("broker_ip")
        identifier: int = int(arguments.get("identifier"))
        seed: int = int(arguments.get("seed"))
        broker_username: str = str(arguments.get("broker_username"))
        broker_password: str = str(arguments.get("broker_password"))
        wait_delay: float = float(arguments.get("wait_delay"))

        AsyncControlable.__init__(
            self,
            broker_ip,
            "Agent" + str(identifier),
            broker_username,
            broker_password,
            wait_delay
        )
        BaseAgent.__init__(self, identifier, seed)

        self.on_initialization()

    def publish(self, topic: str, message) -> None:
        """
        publish a message on the topic
        :param topic: str
        :param message: content of the message
        """
        self.client.publish("agent/" + str(self.id) + "/" + topic, message)

    def behaviour(self) -> None:
        self.on_cycle_begin()
        self.on_perceive()
        self.on_decide()
        self.on_act()
        self.on_cycle_end()
        self.publish("metric", str({"id": self.id, "metric": self.send_metric(), "amas": self.amas_data()}))
        self.publish("canvas", str(self.to_canvas()))

