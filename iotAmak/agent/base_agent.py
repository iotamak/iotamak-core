"""
Base Agent class file
"""
import random
from ast import literal_eval
from typing import Dict, List


class BaseAgent:
    """
    base class for agent
    """

    def __init__(self, identifier: int, seed: int) -> None:
        self.id: int = identifier
        random.seed(seed + 10 + self.id)

        self.neighbors: List[Dict] = []
        self.next_neighbors: List[Dict] = []
        self.subscribe("amas/agent/" + str(self.id) + "/neighbor", self.add_neighbor)

    def on_initialization(self) -> None:
        """
        This method will be executed at the end of __init__()
        """
        pass

    def on_cycle_begin(self) -> None:
        """
        This method will be executed at the start of each cycle
        """
        pass

    def on_perceive(self) -> None:
        """
        Method that should be used to open the neighbor metrics and use them
        """
        pass

    def on_decide(self) -> None:
        """
        Should be override
        """
        pass

    def on_act(self) -> None:
        """
        Should be override
        """
        pass

    def on_cycle_end(self) -> None:
        """
        This method will be executed at the end of each cycle
        """
        pass

    def add_neighbor(self, client, userdata, message) -> None:
        """
        Called when the agent, receive metrics
        put the metric in a list that will be rad during on_perceive
        param message: metric (dict) of the neighbor
        """
        result: Dict = literal_eval(message.payload.decode("utf-8"))
        self.next_neighbors.append(result)

    def log(self, message: str) -> None:
        """
        Convenient method to log things (will be printed in the amas's stdout
        :param message:
        """
        self.client.publish(
            "agent/" + str(self.id) + "/log",
            "[AGENT] " + str(self.id) + " : " + message
        )

    def to_canvas(self) -> dict:
        """
        To override if the canvas is used, automatically called each cycle.
        the dict must contains :
         * 'x', 'y' keys -> int values
         * 'id' of the agent (self.id) -> int value
         * 'color' in hex -> str (e.g. '#FF0000')
         * 'cycle' self.nbr_cycle -> int
        """
        return {}

    def send_metric(self) -> Dict:
        """
        Should be override if the neighbor need to be aware of any other info, should be a dict
        """
        return {"id": self.id}

    def amas_data(self) -> Dict:
        """
        Should be override if needed, should be a dict
        """
        return {"id": self.id}