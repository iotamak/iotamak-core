from typing import List

import paramiko
import os
import sys
import pathlib

from pexpect import pxssh

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))

from iotAmak.ssh_module.remote_client import RemoteClient

class Cmd:

    def __init__(self, cmd: str, do_print: bool = True, prefix: str = ""):
        self.cmd = cmd
        self.do_print = do_print
        self.prefix = prefix

class SSHClient:

    def __init__(self, clients: List[RemoteClient], iot_path: str):
        self.clients: List[RemoteClient] = clients
        self.iot_path: str = iot_path

    def run_cmd(self, client: int, cmd: list, repeat: bool = False) -> List[str]:
        ret: List[str] = []
        try:
            s = pxssh.pxssh()
            dest = self.clients[client]
            s.login(dest.hostname, dest.user, dest.password)

            for command in cmd:
                s.sendline(command.cmd)
                s.prompt()
                ret.append(s.before.decode('utf-8'))
                if command.do_print:
                    print(command.prefix, ret[-1])

            s.logout()
        except pxssh.ExceptionPxssh as e:
            print("pxssh failed on login.")
            print(e)
            if not repeat:
                self.run_cmd(client, cmd, True)
        return ret

    def update(self, experiment_name: str, path_to_experiment: str):
        for client in self.clients:
            transport = paramiko.Transport((client.hostname, 22))
            transport.connect(username=client.user, password=client.password)
            sftp = MySFTPClient.from_transport(transport)
            sftp.mkdir(self.iot_path + experiment_name, ignore_existing=True)

            sftp.put_dir(
                path_to_experiment,
                self.iot_path + experiment_name
            )
            sftp.close()


class MySFTPClient(paramiko.SFTPClient):
    def put_dir(self, source, target):
        """
        Uploads the contents of the source directory to the target path. The
        target directory needs to exists. All subdirectories in source are
        created under target.
        """
        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                self.put(os.path.join(source, item), '%s/%s' % (target, item))
                print(os.path.join(target, item))
            else:
                if any([i in item for i in [".git", "__pycache__", ".idea", ".vscode"]]):
                    pass
                else:
                    self.mkdir('%s/%s' % (target, item), ignore_existing=True)
                    self.put_dir(os.path.join(source, item), '%s/%s' % (target, item))
                    print(os.path.join(source, item))

    def mkdir(self, path, mode=511, ignore_existing=False):
        """
        Augments mkdir by adding an option to not fail if the folder exists
        """
        try:
            super(MySFTPClient, self).mkdir(path, mode)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise
