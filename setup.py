from setuptools import setup, find_packages

setup(
    name='iotAmak',
    packages=find_packages(),
    version='0.0.8',
    description='AmakFramework in python',
    author='SMAC - GOYON Sebastien',
    install_requires=[
        "paho-mqtt >= 1.6.1",
        "paramiko >= 2.10.4",
        "pexpect >= 4.8.0",
    ],
)
